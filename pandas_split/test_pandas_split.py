import os
import numpy as np
import pandas as pd
from pandas_split import read_all

NUM_COLUMNS = 50
NUM_CLASS_COLUMNS = 20
NUM_CLASSES = 150
NUM_ROWS = 1000
IDX_COLS = 3

FILENAME = "part"
DIR_NOIDX = "data/no_idx"
DIR_IDX = "data/idx"

for dir_ in [DIR_NOIDX, DIR_IDX]:
    if not os.path.exists(dir_):
        os.makedirs(dir_)


def generate_files(idx=False):
    if idx:
        # indices of dataframe are meaningful for in the context of the dataset
        dir_ = DIR_IDX
    else:
        # indices of the dataframe are a list of integers
        dir_ = DIR_NOIDX

    common_dict_list = []
    for i in range(NUM_CLASSES):
        # Saving per file
        df_body_not_common = np.random.rand(
            NUM_ROWS, NUM_COLUMNS - NUM_CLASS_COLUMNS
        )
        not_common_cols = [
            f"c{c}" for c in np.arange(NUM_CLASS_COLUMNS, NUM_COLUMNS)
        ]
        df_not_common = pd.DataFrame(
            data=df_body_not_common,
            columns=not_common_cols,
        )
        common_dict = {f"c{j}": i for j in range(NUM_CLASS_COLUMNS)}
        common_dict_list.append(
            {**common_dict, **{"file": f"{FILENAME}_{i}.pkl"}}
        )

        if idx:
            # Setting IDX_COLS first columns as Dataframe indices
            df_not_common.set_index(not_common_cols[:IDX_COLS], inplace=True)
        df_not_common.to_pickle(f"{dir_}/{FILENAME}_{i}.pkl")

    index = pd.DataFrame(common_dict_list)
    common_dict_idx = [c for c in index.columns if c != "file"]
    if idx:
        # Setting columns that are not "file" as indices of Dataframe
        index.set_index(common_dict_idx, inplace=True)
    index.to_pickle(f"{dir_}/index.pkl")


#### TESTING


def filter_common(d):
    return d["c1"] < 10


def filter_df(d):
    return d["c40"] < 0.1


def test_filters_run():
    generate_files()
    df = read_all(
        DIR_NOIDX,
        filter_common=filter_common,
        filter_df=filter_df,
        df_columns=["c1", "c3", "c30", "c40"],
    )
    assert list(df.columns) == ["c1", "c3", "c30", "c40"]


def test_shape():
    generate_files()
    df = read_all(DIR_NOIDX)
    assert df.shape == (NUM_CLASSES * NUM_ROWS, NUM_COLUMNS)


# Test for when the indices of the dataframes are meaningful
# of the dataset variables/categories
def test_filters_run_idx():
    generate_files(idx=True)
    df = read_all(
        DIR_IDX,
        filter_common=filter_common,
        filter_df=filter_df,
        df_columns=["c30", "c40"],
    )
    assert list(df.columns) == ["c30", "c40"]


def test_shape_idx():
    generate_files(idx=True)
    df = read_all(DIR_IDX)
    assert df.shape == (
        NUM_CLASSES * NUM_ROWS,
        NUM_COLUMNS - IDX_COLS - NUM_CLASS_COLUMNS,
    )
